# Import and initialize the pygame library
from sprites.tower import Tower
from sprites import game_state, grid
import pygame
from sprites import enemy

# Define constants for the screen width and height
SCREEN_WIDTH = 500
SCREEN_HEIGHT = 500

pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))

# Setup the clock for a decent framerate
clock = pygame.time.Clock()

my_game_state = game_state.GameState()

# Create a custom event for adding a new enemy
ADDENEMY = pygame.USEREVENT + 1
pygame.time.set_timer(ADDENEMY, 5000)

# Create groups to hold enemy sprites and all sprites
# - enemies is used for collision detection and position updates
# - all_sprites is used for rendering
enemies = pygame.sprite.Group()
towers = pygame.sprite.Group()
all_sprites = pygame.sprite.Group()
# all_sprites.add(player)

grid = grid.Grid(grid.DEFAULT_MAP)

# Init font
pygame.font.init()
myfont = pygame.font.SysFont("Comic Sans MS", 30)


def buy_tower(mouse_x, mouse_y):
    row, col = grid.convert_pos_from_px(mouse_x, mouse_y)
    # TODO: Check if can place on grid or not (can't place on path)
    # TODO: If that row, col contain tower, don't create tower
    tower = Tower(row, col, grid)
    towers.add(tower)
    all_sprites.add(tower)
    print("[GAME]: Tower bought!!!")


# Run until the user asks to quit
running = 1
while running:
    # Loop all event in the queue
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = 0

        # Handle when new enemy added
        if event.type == ADDENEMY:
            # Create a new enemy and add it to sprite group
            new_enemy = enemy.Enemy(grid, my_game_state)
            enemies.add(new_enemy)
            all_sprites.add(new_enemy)

        # Create Tower when left click
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            mouse_x, mouse_y = pygame.mouse.get_pos()
            print(mouse_x, mouse_y)
            buy_tower(mouse_x, mouse_y)

    # Update enemies
    enemies.update()

    # Update towers
    towers.update(enemies)

    # Fill Entire screen with black
    screen.fill((0, 0, 0))

    # Draw Map
    screen.blit(grid.surf, grid.rect)

    # Draw all sprites on Screen
    for entity in all_sprites:
        screen.blit(entity.surf, entity.rect)

    # TODO: Draw UI HP, GOLD, ENEMY COUNT
    hp_surface = myfont.render(str(my_game_state.hp), False, (244, 41, 65))
    screen.blit(hp_surface, (460, 0))

    # TODO: Stop if game over (check from game_state)
    if my_game_state.game_over:
        gameover_surface = myfont.render("GAME OVER", False, (0, 0, 0))
        screen.blit(
            gameover_surface,
            (
                SCREEN_WIDTH / 2 - gameover_surface.get_width() / 2,
                SCREEN_HEIGHT / 2 - gameover_surface.get_height() / 2,
            ),
        )

    # Flip the display
    pygame.display.flip()

    # Ensure program maintains a rate of 30 frames per second
    clock.tick(30)
# Close game
pygame.quit()
