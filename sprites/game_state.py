class GameState:
    def __init__(self):
        self.hp = 50
        self.kill = 0
        self.gold = 0
        self.game_over = False

    def add_hp(self, amount):
        self.hp += amount
        if self.hp <= 0:
            self.game_over = True
            self.hp = 0

    def is_game_over(self):
        return self.game_over
