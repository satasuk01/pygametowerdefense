from sprites.grid import Grid, Direction
import pygame
from .entity import Entity


class Tower(Entity):
    def __init__(self, row: int, col: int, grid: Grid):
        super(Tower, self).__init__()
        self.surf = pygame.Surface((20, 20))
        self.surf.fill((0, 0, 0))
        self.grid = grid
        self.row = row
        self.col = col

        center_x, center_y = self.grid.get_pos_px(row, col)
        self.rect = self.surf.get_rect(center=(center_x, center_y))

        self.damage = 25
        self.atk_speed = 10.0
        self.sell_price = 1
        self.range = 50  # px

        self.rect_range = pygame.Rect(
            self.rect.left - self.range,
            self.rect.top - self.range,
            self.rect.bottom + self.range,
            self.rect.right + self.range,
        )

        self._attack_counter = 100.0

    def destroy(self):
        self.kill()

    def update(self, enemies: list):
        # Attack a single enemy
        if self._attack_counter >= 100.0:
            for enemy in enemies:
                is_collide = self.rect_range.collidepoint(enemy.rect.x, enemy.rect.y)
                if is_collide:
                    enemy.add_hp(-self.damage)
                    break
            self._attack_counter = 0.0
        else:
            self._attack_counter += self.atk_speed
