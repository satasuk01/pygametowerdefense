from sprites.game_state import GameState
from sprites.grid import Grid, Direction
import pygame
from .entity import Entity

# TODO: Draw HP over monster


class Enemy(Entity):
    def __init__(
        self,
        grid: Grid,
        game_state: GameState,
        row: int = 1,
        col: int = 0,
        hp: int = 100,
        damage: int = 1,
        gold: int = 1,
        speed: float = 5,
    ):
        super(Enemy, self).__init__()
        self.surf = pygame.Surface((20, 20))
        self.surf.fill((255, 0, 0))
        self.rect = self.surf.get_rect(center=(0 + 12.5, 25 + 12.5))

        self.hp = hp
        self.damage = damage
        self.gold = gold
        self.speed = speed

        self.next_x = -1
        self.next_y = -1

        # enemy position
        self.row = row
        self.col = col
        self.direction = 0

        # Use Grid to navigate enemy
        self.grid = grid

        self.game_state = game_state

    def get_pos(self) -> tuple():
        """get current position of the middle of the enemy in pixels(int)"""
        # WARNING centerx, centery are integers.
        return (self.rect.centerx, self.rect.centery)

    def _update_path(self):
        pos, direction = self.grid.get_next_pos(self.row, self.col)
        self.next_x = pos[0]
        self.next_y = pos[1]
        self.direction = direction

        # x, y = self.grid.get_pos_px(self.row, self.col)
        # print(x, y)
        # self.rect.x = x
        # self.rect.y = y
        # self.row, self.col = self.grid.convert_pos_from_px(self.next_x, self.next_y)

    def add_hp(self, amount):
        self.hp += amount
        if self.hp <= 0:
            self.game_state.gold += self.gold
            self.kill()

    def _move(self):
        if self.direction == Direction.UP:
            self.rect.move_ip(0, -self.speed)
            if self.get_pos()[1] < self.next_y:
                self.direction = 0
                self.rect = self.surf.get_rect(center=(self.rect.centerx, self.next_y))

        elif self.direction == Direction.DOWN:
            self.rect.move_ip(0, self.speed)
            if self.get_pos()[1] > self.next_y:
                self.direction = 0
                self.rect = self.surf.get_rect(center=(self.rect.centerx, self.next_y))

        elif self.direction == Direction.LEFT:
            self.rect.move_ip(-self.speed, 0)
            if self.get_pos()[0] < self.next_x:
                self.direction = 0
                self.rect = self.surf.get_rect(center=(self.next_x, self.rect.centery))

        elif self.direction == Direction.RIGHT:
            self.rect.move_ip(self.speed, 0)
            if self.get_pos()[0] > self.next_x:
                self.direction = 0
                self.rect = self.surf.get_rect(center=(self.next_x, self.rect.centery))

        elif self.direction == Direction.CORE:
            self.game_state.add_hp(-self.damage)
            # By access self.game_state
            self.kill()
            return

        # Update row, col
        current_x, current_y = self.get_pos()
        self.row, self.col = self.grid.convert_pos_from_px(current_x, current_y)

    def update(self):
        if self.direction == 0:
            self._update_path()

        self._move()
        # By using function from grid like get_pos_px
        # self.rect.move_ip(self.speed, 0)
