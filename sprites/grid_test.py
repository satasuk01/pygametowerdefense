from grid import Grid

map = [[0] * 20] * 15
# print(map)
my_grid = Grid(map)
assert my_grid.get_pos_px(0, 0) == (12.5, 12.5)
assert my_grid.convert_pos_from_px(20, 12) == (0, 0)
