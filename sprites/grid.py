import pygame

DEFAULT_MAP = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [2, 2, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 5],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
]


class Direction:
    UP = 1
    RIGHT = 2
    DOWN = 3
    LEFT = 4
    CORE = 5


class Grid(pygame.sprite.Sprite):
    def __init__(self, map: list, block_size: float = 25):
        """map size of 20x15 (width x height)"""
        self.map = map
        self.block_size = block_size

        self.surf = pygame.Surface((500, 375))
        self.surf.fill((127, 187, 37))  # Fill Green
        self.draw_path()

        self.rect = self.surf.get_rect(center=(500 / 2, 375 / 2))

    def draw_path(self):
        """Draw white color to self.surf"""
        # TODO: Draw a path by number
        # pygame.draw.rect(self.surf, (213, 196, 161), (0, 25, 25, 25))
        for i in range(len(self.map)):
            for j in range(len(self.map[0])):
                if self.map[i][j] != 0:
                    pygame.draw.rect(
                        self.surf,
                        (213, 196, 161),
                        (25 * j, 25 * i, 25, 25),
                    )
                if self.map[i][j] == 5:
                    # Draw pink
                    pygame.draw.rect(
                        self.surf,
                        (219, 48, 130),
                        (25 * j + 5, 25 * i + 5, 18, 18),
                    )

    def get_pos_px(self, row: int, col: int) -> tuple:
        """Caculate Position from grid index for screen 500x375px"""
        if row >= 15 or col >= 20:
            raise Exception(
                f"Grid.get_position_px: row or col out of range ({row},{col})"
            )

        x_position = self.block_size * col + self.block_size / 2
        y_position = self.block_size * row + self.block_size / 2
        return (x_position, y_position)

    def convert_pos_from_px(self, x: float, y: float) -> tuple:
        """Calculate row, col from given pos x,y"""
        col = x // self.block_size
        row = y // self.block_size

        return (row, col)

    def get_next_pos(self, row: int, col: int) -> tuple:
        """convert row , col to next pixel position"""
        if self.map[row][col] == Direction.UP:
            return self.get_pos_px(row - 1, col), Direction.UP
        elif self.map[row][col] == Direction.DOWN:
            return self.get_pos_px(row + 1, col), Direction.DOWN
        elif self.map[row][col] == Direction.LEFT:
            return self.get_pos_px(row, col - 1), Direction.LEFT
        elif self.map[row][col] == Direction.RIGHT:
            return self.get_pos_px(row, col + 1), Direction.RIGHT

        return self.get_pos_px(0,0), Direction.CORE
        # raise Exception("Grid.get_next_pos: Error")
