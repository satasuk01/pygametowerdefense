import pygame


class Entity(pygame.sprite.Sprite):
    def __init__(self):
        super(Entity, self).__init__()
        self.position_x = 0.0
        self.position_y = 0.0
        self.width = 0.0
        self.height = 0.0
        self.surf = None
        self.rect = None

    def update(self):
        pass
